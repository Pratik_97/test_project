package ordermoduletests.Tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import ordermoduletests.DriverSingleton;
import ordermoduletests.Pages.Loginpage;
import ordermoduletests.Pages.Ndrpage;
import ordermoduletests.Pages.Placeorderpage;

public class Ndrtests {

    WebDriver driver;

    @BeforeSuite
    public void setup() {
        DriverSingleton ds_obj = DriverSingleton.getInstance();
        driver = ds_obj.getDriver();
    }

    @Test (priority = 0)
    public void verifyNdrPageOpens() throws InterruptedException {
        Placeorderpage placeorder_var = new Placeorderpage(driver);
        Loginpage loginpage_obj = new Loginpage(driver);

        placeorder_var.navigatetosellerdash();
        Thread.sleep(3000);
        loginpage_obj.enter_phonenumber("9436582726");
        loginpage_obj.click_sendotp();
        Thread.sleep(3000);
        loginpage_obj.enter_otp("0000");
        loginpage_obj.click_verifyotp();
        Thread.sleep(3000);
        Ndrpage ndr = new Ndrpage(driver);
        ndr.navigateToNdr();
        Thread.sleep(3000);
        ndr.hoverNdr();
        Thread.sleep(3000);
        ndr.clickNdr();

    }

    @AfterSuite
    public void teardown(){
        driver.close();
    }

}
