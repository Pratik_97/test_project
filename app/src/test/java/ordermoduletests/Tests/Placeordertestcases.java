package ordermoduletests.Tests;

import java.time.Duration;
import org.checkerframework.checker.units.qual.s;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ordermoduletests.DriverSingleton;
import ordermoduletests.Pages.Loginpage;
import ordermoduletests.Pages.Placeorderpage;

public class Placeordertestcases {

    WebDriver driver_var;
    Placeorderpage placeorder_var;
    Loginpage loginpage_obj;

    @BeforeSuite
    public void setup() {
        DriverSingleton ds_obj = DriverSingleton.getInstance();
        driver_var = ds_obj.getDriver();
    }

    @Test (priority = 0)
    public void verifyplaceorderisdisplayed() throws InterruptedException {
        placeorder_var = new Placeorderpage(driver_var);
        loginpage_obj = new Loginpage(driver_var);
        placeorder_var.navigatetosellerdash();
        Thread.sleep(3000);
        loginpage_obj.enter_phonenumber("9436582726");
        loginpage_obj.click_sendotp();
        Thread.sleep(3000);
        loginpage_obj.enter_otp("0000");
        loginpage_obj.click_verifyotp();
        Thread.sleep(3000);
        placeorder_var.hoverto_order();
        Thread.sleep(3000);
        placeorder_var.clickplaceorder();
        Thread.sleep(3000);
        Assert.assertTrue(placeorder_var.checkIftextdisplayed());
    }

    @Test (priority = 1)
    public void verifybulkorderisdisplayed() throws InterruptedException {
        placeorder_var = new Placeorderpage(driver_var);
        Assert.assertTrue(placeorder_var.checkifbulkordertextisdisplayed());
    }

    @Test (priority = 1)
    public void verifyerrormessageinplaceorder() {
        placeorder_var = new Placeorderpage(driver_var);
        Assert.assertTrue(placeorder_var.errormessage());
    }

    @Test (priority = 1)
    public void verifyNextbuttonRedirectsToproductpage() {
        placeorder_var = new Placeorderpage(driver_var);
        // placeorder_var.clicknextbutton();
        Assert.assertTrue(false);

    }

    @AfterSuite
    public void teardown() {
        driver_var.close();
    }

}
