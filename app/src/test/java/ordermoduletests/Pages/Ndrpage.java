package ordermoduletests.Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Ndrpage {

    @FindBy(xpath = "//div[normalize-space()='NDR']")
    WebElement ndrElement;

    @FindBy(xpath = "//nav//div[3]")
    WebElement side_bar;

    WebDriver driver;

    Actions act;

    String url = "http://v2.nushop-dashboard.kaip.in/";

    public Ndrpage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void navigateToNdr() {
        driver.get(url);
    }

    public void hoverNdr() {

        try {
            
            act = new Actions(driver);
            act.moveToElement(side_bar).click().perform();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void clickNdr() {
        ndrElement.click();
    }

}
